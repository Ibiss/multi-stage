###Configuration
FROM golang:alpine AS config
LABEL image=config

RUN apk update && apk upgrade && \
apk add --no-cache bash git openssh 

# Configure SSH
RUN /bin/bash -l -c "mkdir /root/.ssh"

RUN ssh-keyscan github.com >> ~/.ssh/known_hosts

ADD .ssh/id_rsa /root/.ssh/id_rsa

RUN chmod 700 /root/.ssh/id_rsa

RUN echo "Host github.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

RUN /bin/bash -l -c "mkdir /home/workspace"
WORKDIR /home/workspace
RUN git clone git@github.com:Woap/go-example.git .

###Builder
FROM config AS builder
ARG build
LABEL build=${build}
LABEL image=builder
RUN go build -o hello


###Deploy

FROM alpine AS deploy
LABEL image=deploy
COPY --from=builder /home/workspace/hello ./

ENTRYPOINT /hello
